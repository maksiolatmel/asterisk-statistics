<div id="LeftPanel" style="width: auto; padding: 10px; ">    
<!-- Tab links -->
<div class="tab">
  <p style="float:left; color:white;margin-left: 20px; font-size: 14px;">История звонков</p> 
  <button id="BLost" class="tablinks" onclick="openCity(event, 'Lost')">Пропущенные</button>
  <button id="BPerfect" class="tablinks" onclick="openCity(event, 'Perfect')">Исходящие</button>
  <button id="BReceived" class="tablinks" onclick="openCity(event, 'Received')" >Входящие</button>
</div> 

<!-- ************************ Совершенные звонки (Исходящие) **********************************-->
<div id="Perfect" class="tabcontent">
<table  id="TPerfect" align="center" cellspacing="0" cellpadding="1" width="100%" border="1" style="text-align:left;vertical-align: middle; margin-top:20px; " >
    <thead>
    <tr Class="THead">
        <td align="center" style="width:20%; vertical-align: middle;">Абонент</td>
        <td align="center" style="width:20%; vertical-align: middle;">Оператор</td>
        <td align="center" style="width:5%; vertical-align: middle;">Время ожидания (с)</td>
        <td align="center" style="width:5%; vertical-align: middle;">Длительность (с)</td>
        <td align="center" style="width:30%; vertical-align: middle;">Дата время</td>
        <td align="center" style="width:30px; vertical-align: middle;"><img src="img\Play.png"></td> 
        <td align="center" style="width:10%; vertical-align: middle;">Статус звонка</td>
    </tr>
    </thead>
     <tbody>
<?php 
    $res = $dbh->query("SELECT * FROM asteriskcdrdb.cdr where dcontext='from-internal'  ORDER BY calldate DESC LIMIT 500");
    if($res) {
        while ($line = $res->fetch(PDO::FETCH_ASSOC)){
            echo '<tr Class="TBody">
            <td>'.$line[clid].'</td>
            <td>'.$line[dst].'</td>
            <td>'.$line[duration].'</td>
            <td>'.$line[billsec].'</td>
            <td>'.$line[calldate].'</td>
            <td>'.Player(1,$line[clid],$line[dst],$line[calldate],$line[uniqueid]).'</td>
            <td>'.$line[disposition].'</td>
            </tr>';
        }
    }
?>    
     </tbody>
  </table>  
</div>

<!-- ************************ Принятые звонки (Входящие)**********************************-->

<div id="Received" class="tabcontent">
  
<table  id="TReceived" align="center" cellspacing="0" cellpadding="1" width="100%" border="1" style="text-align:center;vertical-align: middle; margin-top:20px;" >
    <thead>
    <tr Class="THead">

        <td align="center" style="width:20%; vertical-align: middle;">Абонент</td>
        <td align="center" style="width:10%; vertical-align: middle;">Оператор</td>
        <td align="center" style="width:5%; vertical-align: middle;">Время ожидания (с)</td>
        <td align="center" style="width:5%; vertical-align: middle;">Длительность (с)</td>
        <td align="center" style="width:30%; vertical-align: middle;">Дата и время</td>
        <td align="center" style="width:30px; vertical-align: middle;"><img src="img\Play.png"></td> 
        <td align="center" style="width:10%; vertical-align: middle;">Разорвал соединение</td> 
        <!--<td align="center" style="width:5%; vertical-align: middle;">uniqueid</td>-->

    </tr>
    </thead>
    <tbody>
<?php
    
    $res = $dbh->query("SELECT * FROM qstatslite.queue_stats where (qevent = 7 OR qevent = 8)ORDER BY queue_stats_id DESC LIMIT 500 ");
    if($res) {
        while ($line = $res->fetch(PDO::FETCH_ASSOC)){
            $Ansv = GetForReceived($dbh,$line[uniqueid]);
            $Operator = $Agents[$Ansv[Operator]];
            echo '<tr Class="TBody">
                <td>'.$Ansv[Number_In].'</td>
                <td>'.$Operator.'</td>
                <td>'.$Ansv[Wait].'</td>
                <td>'.$Ansv[Speak].'</td>
                <td>'.$Ansv[Call_Date].'</td>
                <td>'.Player(2,$Operator,$Ansv[Number_In],$Ansv[Call_Date],$line[uniqueid]).'</td>
                <td>'.$Ansv[end].'</td>

                </tr>';
        }
    }


function GetForReceived($PD,$id){
    $Ares = $PD->query("SELECT * FROM qstatslite.queue_stats where uniqueid = ".$id);
    $Ansv = Array();
    if($Ares) {
        while ($line = $Ares->fetch(PDO::FETCH_ASSOC)){
            if($line[qevent] == 11){
                $Ansv[Number_In] = $line[info2];
                $Ansv[Call_Date] = $line[datetime];
            }
            if(($line[qevent] == 7) ||($line[qevent] == 8)){
                    $Ansv[Operator] = $line[qagent];
                    $Ansv[Wait] = $line[info1];
                    $Ansv[Speak] = $line[info2];
                    if($line[qevent] == 7) $Ansv[end] = 'сотрудник';
                    if($line[qevent] == 8) $Ansv[end] = 'абонент';
            }
        }
    } else $Ansv = 'Error';
    return $Ansv;
}
  
?>    
    </tbody>
  </table> 
</div>

<!-- *************************** Потерянные звонки **********************-->


<div id="Lost" class="tabcontent">

<table  id="TLost" align="center" cellspacing="0" cellpadding="1" width="100%" border="1" style="text-align:left;vertical-align: middle; margin-top:20px;" >
    <thead>
    <tr Class="THead">
 
        <td align="center" style="width:10%; vertical-align: middle;">Абонент</td>
        <td align="center" style="width:10%; vertical-align: middle;">Оператор (группа)</td>
        <td align="center" style="width:10%; vertical-align: middle;">Время ожидания (с)</td>
        <td align="center" style="width:30%; vertical-align: middle;">Дата и время</td>

        <td align="center" style="width:20%; vertical-align: middle;">uniqueid</td>

    </tr>
    </thead>
    <tbody>
<?php
    
    $res = $dbh->query("SELECT * FROM qstatslite.queue_stats where qevent = 1 ORDER BY queue_stats_id DESC LIMIT 500");
    if($res) {
        while ($line = $res->fetch(PDO::FETCH_ASSOC)){
 
                    $Ansv = GetForLost($dbh,$line[uniqueid]);
                    
                    echo '<tr Class="TBody">
                    <td>'.$Ansv[Number_In].'</td>
                    <td>'.$Names[$Ansv[Operator]].'</td>
                    <td>'.$Ansv[Wait].'</td>
                    
                    <td>'.$Ansv[Call_Date].'</td>
                    <td>'.$line[uniqueid].'</td>
                    </tr>';
        }
    }

function GetForLost($PD,$id){
    $Ares = $PD->query("SELECT * FROM qstatslite.queue_stats where uniqueid = ".$id);
    $Ansv = Array();
    if($Ares) {
        while ($line = $Ares->fetch(PDO::FETCH_ASSOC)){
            if($line[qevent] == 11){
                $Ansv[Number_In] = $line[info2];
                $Ansv[Call_Date] = $line[datetime];
            }
            if($line[qevent] == 1){
                    $Ansv[Operator] = $line[qname];
                    $Ansv[Wait] = $line[info3];
                    $Ansv[Speak] = $line[info2];
            }
        }
    } else $Ansv = 'Error';
    return $Ansv;
}    

function Player($dir,$operator,$phone,$dt,$uni){
	//$FName = $dt.'  '.$uni;
	if($dir == 1) $Direction = 'exsternal';
	else
	if($dir == 2) $Direction = 'out';
	list($Ddate,$Dtime) = explode(' ', $dt);
	//$FName = 'Date = '.$Ddate.' Time = '.$Dtime;
	list($year,$month,$day) = explode('-', $Ddate);
	list($Hour,$Min,$Sec) = explode(':', $Dtime);
	$path = 'var/spool/asterisk/minitor/'.$year.'/'.$month.'/'.$day.'/';
	$Fpath = $Direction.'-'.$operator.'-'.$year.$month.$day.'-'.$Hour.$Min.$Sec.'-'.$uni.'WAV';
	$FName = $path.$Fpath;
	//$FName = 'Year = '.$year.' Month = '.$month.' Day = '.$day.' Hour = '.$Hour.' Min = '.$Min.' Sec = '.$Sec;

	$out = '<div class="Player" title="'.$FName.'">
				<button></button>
				<audio preload="metadata">
  					<source src="audio\1.mp3" type="audio/mpeg">
				</audio>
			</div>';
	return $out;			
}
?>    
    </tbody>
  </table>   

</div>

</div>